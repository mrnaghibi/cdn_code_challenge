##### The purpose of this repository
###### The first 1000 people to enter the discount code will win a gold coin

Beside Laravel, this project uses other tools like:

- [laradock](https://laradock.io/)
    - [Redis-Cluster](https://redis.io/topics/cluster-tutorial)
    - [nginx](https://www.nginx.com/)
    - [php-fpm](https://php-fpm.org/)
    - [mysql](https://www.mysql.com/)
- [Distributed locks with Redis - Atomic Locks](https://laravel.com/docs/7.x/cache#managing-locks)
- [Vue.js](https://vuejs.org/)
- [Bootstrap 4](https://getbootstrap.com/)
- [axios](https://github.com/mzabriskie/axios)

## Some screenshots

You can find some screenshots of the application on : [https://imgur.com/a/564gS4m](https://imgur.com/a/564gS4m)

## Installation

Development environment requirements :
- [Docker](https://www.docker.com) >= 19.03 CE
- [Docker Compose](https://docs.docker.com/compose/install/)

Setting up your development environment on your local machine :
```bash

$ git clone https://gitlab.com/mrnaghibi/cdn_code_challenge.git
$ cd cdn_code_challenge/app_code
$ cp .env.example .env
$ cd cdn_code_challenge/docker
$ cp .env.example .env
$ append 127.0.0.1 cdn-arvan.test to /etc/hosts file
$ docker-compose up -d
$ docker-compose exec  --user=laradock workspace bash
$ php artisan key:generate
$ composer install

```
Now you can access the application via [cdn-arvan.test](cdn-arvan.test).


## Before starting
You need to run the migrations with the seeds :

```bash
$ docker-compose exec --user=laradock workspace bash
$ php artisan migrate --seed
```

And then, compile the assets :
```bash
$ docker-compose exec --user=laradock workspace bash
$ npm run dev
```

## Accessing the API

- enter a discount code
```
POST: api/redeem
body
    code:
    mobile:
```

## Scale the App
```bash
$ docker-compose up --scale php-fpm=3 -d nginx
```

## Running JMeter jmx file
1. Set the correct path for users.csv file in CDN.jmx file
2. Open command prompt
3. Go into JMeter’s bin folder
4. Enter following command, jmeter -n -t path-to-CDN.jmx -l path-to-results.jtl
