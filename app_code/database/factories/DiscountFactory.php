<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Discount;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Discount::class, function (Faker $faker) {
    return [
        'code' => $faker->word(),
        'description' => $faker->paragraph(1),
        'number' => $faker->numberBetween($min = 100, $max = 1000)
    ];
});
