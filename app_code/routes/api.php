<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('redeem', 'Api\DiscountController');

Route::namespace('Panel')->prefix('panel')->group(function ($router){
    Route::resource('discounts', 'DiscountController')->only([
        'index', 'show','store'
    ]);
    $router->get('/discounts/{discount}/publish','DiscountController@publish')->name('discount.publish');
});


