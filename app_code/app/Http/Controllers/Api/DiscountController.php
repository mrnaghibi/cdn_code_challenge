<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRedeem;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class DiscountController extends Controller
{
    public function __invoke(StoreRedeem $request)
    {
        $discountCode = $request->code;
        $mobile = $request->mobile;
        $result = 'Loser';
        if (Redis::decr($discountCode) >= 0)
            if (Redis::sadd($discountCode . config('app.REDIS_POSTFIX_DISCOUNT_USERS'), $mobile))
                $result = "Winner";
            else
                Redis::incr($discountCode);

        /*  Cache::lock($discountCode . config('app.REDIS_POSTFIX_DISCOUNT_LOCK'), 1)->block(5, function () use (&$result, $discountCode, $mobile) {
              if (Redis::get($discountCode) > 0) {
                  if (Redis::sadd($discountCode . config('app.REDIS_POSTFIX_DISCOUNT_USERS'), $mobile)) {
                      Redis::decr($discountCode);
                  };
                  $result = "Winner";
              }
          });*/
        return response()->json(['data' => $result])
            ->setStatusCode(Response::HTTP_OK);
    }
}
