<?php

namespace App\Http\Controllers\Panel;

use App\Discount;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDiscount;
use App\Http\Resources\DiscountResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Validation\ValidationException;

class DiscountController extends Controller
{
    public function index()
    {
        $discounts = Discount::all();
        return response()->json(['data' => DiscountResource::collection($discounts)])
            ->setStatusCode(Response::HTTP_OK);
    }

    public function store(StoreDiscount $request)
    {
        $discount = Discount::create($request->only('code','number','description'));
        //refresh the model because of eloquent doesnt return a state default value
        $discount = $discount->refresh();
        return response()->json(['data' => new DiscountResource($discount)])
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Discount $discount)
    {
        return response()->json(['data' => new DiscountResource($discount)])
            ->setStatusCode(Response::HTTP_OK);
    }

    public function publish(Discount $discount)
    {
        if ($discount->number <= 0) {
            throw ValidationException::withMessages([
                'number' => 'Discount numbers needs to be greater than zero'
            ]);
        }
        if ($discount->state) {
            throw ValidationException::withMessages([
                'publish' => 'Discount Previously published'
            ]);
        }
        $discount->state = Discount::STATE_PUBLISHED;
        $discount->save();
        Redis::set($discount->code, $discount->number);
//        Redis::expire($discount->code.config('app.REDIS_POSTFIX_DISCOUNT_USERS'), 0);
        Redis::del($discount->code.config('app.REDIS_POSTFIX_DISCOUNT_USERS'));
        return response()->json(['data' => 'Publish Successfully'])
            ->setStatusCode(Response::HTTP_OK);
    }
}
