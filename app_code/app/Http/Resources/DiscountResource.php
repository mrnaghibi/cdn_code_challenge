<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Redis;

class DiscountResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $users = $this->users();
        return [
            'id' => $this->id,
            'code' => $this->code,
            'description' => $this->description,
            'number' => $this->number,
            'state' => $this->state,
            'remaining' => $this->number - count($users),
            'users' => $users
        ];
    }
}
