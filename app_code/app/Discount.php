<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class Discount extends Model
{
    const STATE_DRAFT = 0;
    const STATE_PUBLISHED = 1;

    protected $fillable = [
        'code', 'description', 'number', 'state'
    ];
    public function users()
    {
        return Redis::smembers($this->code . config('app.REDIS_POSTFIX_DISCOUNT_USERS'));
    }
}
