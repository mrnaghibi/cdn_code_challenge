<?php


namespace App\Exceptions;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

trait ExceptionTrait
{
    public function apiException($request, $e)
    {
        if ($e instanceof ModelNotFoundException)
            return $this->response($e->getMessage(), Response::HTTP_NOT_FOUND);
        if ($e instanceof DiscountException)
            return $this->response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY);
        if ($e instanceof ValidationException) {
            return $this->response(Arr::first(Arr::first($e->validator->errors()->getmessages())), Response::HTTP_UNPROCESSABLE_ENTITY);

        }
        return parent::render($request, $e);
    }

    private function response($message, $code)
    {
        return response()->json([
            'data' => null,
            'meta' => [
                'error' => $message
            ]
        ])->setStatusCode($code);
    }
}
