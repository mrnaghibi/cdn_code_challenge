import Vue from "vue";
import Vuex from "vuex";


Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        discounts: []
    },
    getters: {
        discounts(state) {
            return state.discounts;
        }
    },
    mutations: {
        updateDiscounts(state, payload) {
            state.discounts = payload;
        },
        addDiscounts(state, payload){
            console.log(payload)
            state.discounts.push(payload);
        }
    },
    actions: {
        getDiscounts(context) {
            axios.get('/api/panel/discounts')
                .then((response) => {
                    context.commit('updateDiscounts', response.data.data);
                })
        }
    }
});
