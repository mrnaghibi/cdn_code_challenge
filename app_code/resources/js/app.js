require('./bootstrap');

import Vue from "vue";
import router from "./router";
import store from "./store";
import MainApp from "./components/MainApp.vue";


const app = new Vue({
    el: '#app',
    components:{
        MainApp
    },
    router,
    store
});
