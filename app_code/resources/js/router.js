import Vue from "vue";
import VueRouter from "vue-router";
import Discounts from "./components/Discounts";
import List from "./components/List";
import New from "./components/New";


Vue.use(VueRouter);


export default new VueRouter({
    routes: [
        {
            path: '/discounts',
            component: Discounts,
            children: [
                {
                    path: '/',
                    component: List
                },
                {
                    path: 'new',
                    component: New
                }
            ]
        }
    ],
    mode: 'history'
});
